function Q = Q_Learning(S,A,stateS2X,stateX2S,Parameters)
%Q_LEARNING solves for an optimal action value function using the
%Q-Learning algorithm (script section 2.10)
%
% Inputs:   S -- Vector of natural number state indicies
%           A -- Vector of natural number action indices
%           stateS2X -- Function to convert state indices to their physical
%               x,y value
%           stateX2S -- Function to convert x,y grid positions to their
%               corresponding state indices
%           Parameters -- Struct containing parameters for the learning
%               algorithm. Suggested parameters and a set of possible 
%               values provided in main_ex2a.m
%
% Outputs:  Q -- Action value function of size [length(S),length(A)]. Note
%               that the resulting policy is not returned. The policy
%               should be naturally encoded in Q.
%

%% Initialize Variables


%Create object for incremental plotting of reward after each episode
windowSize = 10; %Sets the width of the sliding window fitler used in plotting
plotter = RewardPlotter(windowSize);


%% Q-Learning Algorthim (script section 2.9)
 Q = rand(length(A),length(S));
% Q = zeros(length(A),length(S));
% Q(2,13:24) = 1:12;
% Q(1,1) = 1;
% Q(3,24) = 2;
for TrainLoop = 1:Parameters.training_iterations
    %% Generate a training episode
    cumReward = 0;
    st = stateX2S([4;1]);%S(randi(length(S)));
    isEpisodeEnd = false;
    index_count=1;
    while (index_count<=Parameters.episode_length)&&~isEpisodeEnd %Episode termination criteria
        index_count=index_count+1;
        % Execute the current epsilon-Greedy Policy
        u = currentEpsilonGreedy(st,Q,Parameters.epsilon,stateS2X,stateX2S);

        % Interaction with environment
        %Note that this function takes and returns states expressed by
        %their x,y grid positions. Use the state*2* functions if
        %necessary.
        [xp,reward,isEpisodeEnd] = Cliff_World(stateS2X(st),u);
        st_next = stateX2S(xp);
        
        if isEpisodeEnd
            reward = 10;
        end
        % Log data for the episode
        cumReward = cumReward+reward;
        
        %% Update Q(s,a)
        Q(u,st) = Q(u,st)+Parameters.omega*(reward+Parameters.alpha*maxActionVal(st_next,Q,stateS2X,stateX2S)-Q(u,st));
        st = st_next;
        %stateS2X(st)
    end
    
    
    
    %Update the reward plot
    if mod(TrainLoop,5)==1
        EpisodeTotalReturn = cumReward; %Sum of the reward obtained during the episode
        plotter = UpdatePlot(plotter,EpisodeTotalReturn);
    end
    
    
    %% Decrease the exploration
    %Set k_epsilon = 1 to maintain constant exploration
	Parameters.epsilon = Parameters.epsilon * Parameters.k_epsilon; 
    
end
Q=Q';
end
% function flag = isterminal(S,stateS2X)
% flag = false;
% X = stateS2X(S);
% [x,y] = deal(X(1),X(2));
% if norm([x,y]-[4,12])<0.0001
%     flag = true;    
% end
% end
function u = currentEpsilonGreedy(S,Q,epsilon,stateS2X,stateX2S)
[up,right,down,left] = deal(1,2,3,4);
X = stateS2X(S);
[x,y] = deal(X(1),X(2));
if y<12 %right
    actionValues(right) = Q(right,S);%stateX2S([x;y+1]));
else
    actionValues(right) = -inf;
end
if y>1 %left
    actionValues(left) = Q(left,S);%stateX2S([x;y-1]));
else
    actionValues(left) = -inf;
end
if x<4 %down
    actionValues(down) = Q(down,S);%stateX2S([x-1;y]));
else
    actionValues(down) = -inf;
end
if x>1 %up
    actionValues(up) = Q(up,S);%stateX2S([x+1;y]));
else
    actionValues(up) = -inf;
end
if(rand(1,1)>epsilon)
    u = find(actionValues == max(actionValues));
else
    u = randi(4);
end
end
function val = maxActionVal(S,Q,stateS2X,stateX2S)
[up,right,down,left] = deal(1,2,3,4);
X = stateS2X(S);
[x,y] = deal(X(1),X(2));
if y<12 %right
    actionValues(right) = Q(right,S);%stateX2S([x;y+1]));
else
    actionValues(right) = -inf;
end
if y>1 %left
    actionValues(left) = Q(left,S);%stateX2S([x;y-1]));
else
    actionValues(left) = -inf;
end
if x<4 %down
    actionValues(down) = Q(down,S);%stateX2S([x-1;y]));
else
    actionValues(down) = -inf;
end
if x>1 %up
    actionValues(up) = Q(up,S);%stateX2S([x+1;y]));
else
    actionValues(up) = -inf;
end
val = max(actionValues);
end