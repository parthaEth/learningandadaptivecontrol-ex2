function Q = Monte_Carlo(S,A,stateS2X,stateX2S,Parameters)
%MONTE_CARLO solves for an optimal control policy using the On-Policy Monte
%Carlo method (script section 2.9)
%
% Inputs:   S -- Vector of natural number state indicies
%           A -- Vector of natural number action indices
%           stateS2X -- Function to convert state indices to their physical
%               x,y value
%           stateX2S -- Function to convert x,y grid positions to their
%               corresponding state indices
%           Parameters -- Struct containing parameters for the learning
%               algorithm. Suggested parameters and a set of possible 
%               values provided in main_ex2a.m
%
% Outputs:  Q -- Action value function of size [length(S),length(A)]. Note
%               that the resulting policy is not returned. The policy
%               should be naturally encoded in Q.
%

%% Initialize Variables
%Q = rand(length(S),length(A)); 
Q = zeros(length(S),length(A));
% Q(2,13:24) = 1:12;
% Q(1,1) = 1;
% Q(3,24) = 2;
pi = rand(length(S),length(A));
%Create object for incremental plotting of reward after each episode
windowSize = 10; %Sets the width of the sliding window fitler used in plotting
plotter = RewardPlotter(windowSize);


%% On-Policy Monte Carlo Algorithm (see script section 2.9.3)

for TrainLoop = 1:Parameters.training_iterations
    %% Generate a training episode
    cumReward = 0;
    currState = stateX2S([4;1]);
    isEpisodeEnd = 0;
    i=1;
    action = zeros(Parameters.episode_length,1);
    state  = zeros(Parameters.episode_length,1);
    reward = zeros(Parameters.episode_length,1);
    while (i<=Parameters.episode_length)&& ~isEpisodeEnd %Episode termination criteria
        state(i)=currState;
        % Execute the current epsilon-Greedy Policy
        action(i) = currentEpsilonGreedy(state(i),pi,Parameters.epsilon,stateS2X,stateX2S);

        % Interaction with environment
        %Note that this function takes and returns states expressed by
        %their x,y grid positions. Use the 'state*2*' functions if
        %necessary.
        [xp,reward(i),isEpisodeEnd] = Cliff_World(stateS2X(state(i)),action(i));
        nextState = stateX2S(xp);

        if isEpisodeEnd
            reward(i) = 12;
        end
        % Log data for the episode
        
        
        %% Update Q(s,a)
        
        cumReward = cumReward+reward(i);
        currState = nextState;
        i=i+1;
    end

       state(i)= currState;
       ep_length = i-1;
       state = state(state~=0);
       action = action(action~=0);
       reward = reward(reward~=0);
       for i=1:ep_length
            Q(state(i),action(i)) = Q(state(i),action(i))+Parameters.omega*(sum(reward(i:end))-Q(state(i),action(i)));
       end
    
    %% Monte Carlo Policy Improvement Step
    for i=1:length(state)-1
        actionStar = maxActionVal(state(i),Q,stateS2X,stateX2S);
        for u=1:4
        pi(state(i),u) = updatePI(Parameters.epsilon,u,actionStar); 
        end
    end
    
    %Update the reward plot
    EpisodeTotalReturn = cumReward; %Sum of the reward obtained during the episode
    plotter = UpdatePlot(plotter,EpisodeTotalReturn);
    
    %% Decrease the exploration
    %Set k_epsilon = 1 to maintain constant exploration
	Parameters.epsilon = Parameters.epsilon * Parameters.k_epsilon;
    
    fprintf('Training Iterations: %d Episode Length: %d  Reward: %d \n ',TrainLoop,ep_length,EpisodeTotalReturn);
end

end

function u = currentEpsilonGreedy(S,pi,epsilon,stateS2X,stateX2S)
if(rand(1,1)>epsilon)
    u_temp = find(pi(S,1:4)==max(pi(S,1:4)));
    u = u_temp(1);
else
    u = randi(4);
end

end

function val = maxActionVal(S,Q,stateS2X,stateX2S)
[up,right,down,left] = deal(1,2,3,4);
X = stateS2X(S);
[x,y] = deal(X(1),X(2));
if y<12 %right
    actionValues(right) = Q(S,right);%stateX2S([x;y+1]));
else
    actionValues(right) = -inf;
end
if y>1 %left
    actionValues(left) = Q(S,left);%stateX2S([x;y-1]));
else
    actionValues(left) = -inf;
end
if x<4 %down
    actionValues(down) = Q(S,down);%stateX2S([x-1;y]));
else
    actionValues(down) = -inf;
end
if x>1 %up
    actionValues(up) = Q(S,up);%stateX2S([x+1;y]));
else
    actionValues(up) = -inf;
end
val_temp = find(actionValues==max(actionValues));
val = val_temp(1);
end

function val = updatePI(eps,action,actionStar)
if action==actionStar
    val = 1-eps*(1-(1/4));  
else
    val = eps/4;
end
end
