function [Task,Controller] = MDP_Design(Task,Parameters)
%MDP_Design -- Generate a Markov Decision Process model of the system using
%               discretized state and action spaces.
%
%Inputs:    Task -- specifies the control task (ie Initial Condition and end time)
%           Parameters -- parameters to be used for modeling. Suggested
%               parameters and a set of possible values provided in 
%               main_ex2a.m
%
%Outputs:   Task -- Structure including the following modeling information
%               .S -- List of state indices
%               .A -- List of action idices
%               .P_s_sp_a -- Transition probability distribution. Specifies 
%                   the likelihood of moving from state 's' to state 'sp' 
%                   after applying action 'a'
%               .R_s_a -- Reward function. Expected intermediate reward 
%                   received from applying action 'a' from state 's'
%           Controller -- Discrete controller structure including
%               .state_c2d_handle -- Handle to function which converts a 
%                   set of states in continuous space to their respective 
%                   discretized bins
%               .action_d2c_handle -- Handle to function which convers a 
%                   set of discretized action space bins to their 
%                   corresponding continuous space values
%

%% Step 1: Discretize the state and action spaces

%Important: Note that the provided state_c2d function assumes that the 'X'
%corresponding to each discretized state bin indicates the center point
%of that bin (ie the halfway point between the extremes of position and
%velocity in that bin). If you discretize the state space another way, you
%will also need to modify ths function as well.
discrt_state_pos = linspace(-1.2, +0.5,Parameters.pos_N);
discrt_state_vel = linspace(-0.07,0.07,Parameters.vel_N);
% state_c2d()


X(1,:) = kron(discrt_state_pos,ones(1,Parameters.vel_N));% [2 x (pos_N*vel_N)] matrix of all possible discretized states
X(2,:) = repmat(discrt_state_vel,1,Parameters.pos_N);
Task.S = 1:Parameters.vel_N*Parameters.pos_N; % [1 x (pos_N*vel_N)] list of indices for each corresponding 
              % discretized state value
discrt_action = linspace(-1,+1,Parameters.u_N);
% state_c2d()
U = discrt_action; % [1 x u_N] array of all possible discretized actions
Task.A = 1:Parameters.u_N; % [1 x u_N] array of indices for each corresponding 
                      % discretized action


%Initialize the MDP model of the discretized system
Task.P_s_sp_a = zeros(Parameters.vel_N*Parameters.pos_N,Parameters.vel_N*Parameters.pos_N,Parameters.u_N); % [length(S) x length(S) x length(A)]
Task.R_s_a = zeros(Parameters.vel_N*Parameters.pos_N,Parameters.u_N); % [length(S) x length(A)]


%% Step 2: Generate the discrete state/action space MDP model 
pos_step = abs(X(1,1)-X(1,1+Parameters.vel_N));
vel_step = abs(X(2,1)-X(2,2));

for a = Task.A   % loop over the actions
%disp
    for s = Task.S  % loop over states
        %random ness
        random_change_in_state = [pos_step*(rand(1,Parameters.modeling_iter)-0.5);...
                                  vel_step*(rand(1,Parameters.modeling_iter)-0.5)];
        
        for i = 1:Parameters.modeling_iter % loop over modeling iterations
            
            p0 = X(1,s)+random_change_in_state(1,i);
            v0 = X(2,s)+random_change_in_state(2,i);
            action = U(a);

            
            %Simulate for one time step. This function inputs and returns
            %states expressed by their physical continuous values. You may
            %want to use the included state_*2* functions provided to do
            %this conversion.
            [p1,v1,r,isTerminalState] = Mountain_Car_Single_Step(p0,v0,action);

            if isTerminalState
                if p1>0.49
                    r=10;
                elseif p1<-1.19
                    r=-20;
                end
            end
            %Update the model with the iteration's simulation results
            sp = state_c2d([p1;v1]);
            s1 = state_c2d([p0;v0]);
            Task.P_s_sp_a(s1,sp,a) = Task.P_s_sp_a(s1,sp,a)+(1/Parameters.modeling_iter);
            Task.R_s_a(s1,a) = Task.R_s_a(s1,a)+r/Parameters.modeling_iter;
        end
    end        
end


%% Create the discrete space controller
Controller = struct;
Controller.isDiscrete = true;

%Store handles to the functions used to convert between the discrete and
%continous state and action spaces
Controller.stateC2D = @state_c2d;
Controller.actionD2C = @(A) U(:,A);


    %% Assigning continuous states to the corresponding discrete bin indices
    %***This assumes that X indicates the CENTERPOINT of the bins. If you
    %discretize the states in another way, this function should be changed
    %as well.
    function Sp = state_c2d(Xp)
        % Finding the nearest discrete state bin
        [Sp,~] = knnsearch(X',Xp');
        Sp =Sp';        
    end

end