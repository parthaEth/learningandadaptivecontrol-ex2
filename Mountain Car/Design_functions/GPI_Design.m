function Controller = GPI_Design(Task,Controller,Parameters)
%GPI_DESIGN finds a value function and contol policy for the specified MDP
%using Generalized Policy Iteration (OLCAR script section 2.8)
%
% Inputs:   Task -- Structure containing MDP model. See MDP_Design for
%               description of internal parameters.
%           Controller -- Struct containing function to convert between
%               continuous and discrete parameters. See MDP_Design for a
%               description of internal parameters.
%           Parameters -- Struct containing parameters for the GPI. See
%               main_ex2a.m for a description of the parameters. This can 
%               be modified as desired.
%
% Outputs:  Controller -- Sctruct containing the following
%               .V -- Value function [1 x (N_pos*N_vel)]
%               .Policy -- Optimal control policy found


%% Initialization

%Initialize the policy
Policy =  rand(length(Task.S),length(Task.A));%[length(Task.S) x length(Task.A)]
Policy = Policy./repmat(sum(Policy,2),1,length(Task.A));
% Initialize the value function
V = zeros(length(Task.S),1); % [length(Task.S) x 1]

itr = 0;
while true
    itr = itr+1;
    %% Policy Evaluation (PE) (see script section 2.6)
    for i=1:Parameters.maxIter_PE
        delta = 0;
        for x=Task.S
            v = V(x);
            v_dmy = 0;
            for u=Task.A
                v_dmy = v_dmy+Policy(x,u)*((Task.P_s_sp_a(x,:,u)*V*Parameters.alpha)+Task.R_s_a(x,u));
            end
            V(x) = v_dmy;
            if(abs(v-V(x))>delta)
                delta = abs(v-V(x));
            end
        end
        if delta<Parameters.minDelta_V
            break;
        end
    end
    
    %% Policy Improvment (PI) (see script section 2.7)
    policyIsStable = true;
    for x=Task.S
        b = Policy(x,:);
        value = zeros(1,length(Task.A));
        for u=1:length(Task.A)
            value(u) = (Task.P_s_sp_a(x,:,u)*V+Task.R_s_a(x,u));
        end
        epsilon = 0.001;
        max_val = max(value);
        for u=1:length(Task.A)
            if abs(value(u)-max_val)<epsilon
                Policy(x,u) = 1;
            else
                Policy(x,u) = 0;
            end
        end
        Policy(x,:) = Policy(x,:)/sum(Policy(x,:));
        if norm(b-Policy(x,:))>Parameters.minDelta_Policy
            policyIsStable = false;
        end
    end
    %% Check algorithm convergence
    if policyIsStable || itr>Parameters.maxIter_PI
        break;
    end
end
Controller.V = V';
Controller.Policy = Policy;
end